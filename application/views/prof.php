<div class="container">
  <div class="row col-md">
    
    <div class="col-md-offset-1 col-md-2">
        
        <img src="https://placehold.it/200x200/E8117F/ffffff?text=<?=$user_id;?>" class="circular" alt="image">
        
        <p>
            <a href="https://twitter.com/laddy/" class="button bordered primary">Twitter</a>
            <a href="https://github.com/laddy" class="button bordered primary">Github</a>
            <a href="http://steamcommunity.com/id/laddyicbm" class="button bordered primary">Steam</a>
            <a class="button">Just Button</a>
            <a class="button secondary">aaaaa</a>
            <a class="button secondary large">aaaaa</a>
            <a class="button secondary small">aaaaa</a>
            <a class="button tertiary">bbbbbbb</a>
            <a class="button inverse">cccccccc</a>
        </p>

    </div>
    
    <div class="col-md-8 row">
<!-- 名前、住所、出身、仕事、趣味 -->
        <div class="card fluid">
            <div class="section">
                <h4>aaa</h4>
                <p>私は企画もできるエンジニアです。前職では1000名を超える社員数を持つ会社規模の中、自分のアイデアがベースとなった自社サイトをいくつも生み出してきました。前職では企画部門と開発部門に大きな境目はなく、エンジニアという肩書きの私も企画会議に出席し、月30本ものアイデア出しを2年間続けていました。苦しい日々ではありましたが自分の頭をひねり続ける中で、ユーザー視点がとても養われたと感じています。私が提案した企画が通り開発に至ったサイトも複数あり、社内表彰を過去に2回受賞しています。今後も自社ブランドをもつ貴社でユーザーの目線に立ったものづくりを行っていきたいと考えています。</p>
            </div>
        </div>
        <div class="card fluid">
            <div class="section">
                <h4>aaa</h4>
                <p>私は企画もできるエンジニアです。前職では1000名を超える社員数を持つ会社規模の中、自分のアイデアがベースとなった自社サイトをいくつも生み出してきました。前職では企画部門と開発部門に大きな境目はなく、エンジニアという肩書きの私も企画会議に出席し、月30本ものアイデア出しを2年間続けていました。苦しい日々ではありましたが自分の頭をひねり続ける中で、ユーザー視点がとても養われたと感じています。私が提案した企画が通り開発に至ったサイトも複数あり、社内表彰を過去に2回受賞しています。今後も自社ブランドをもつ貴社でユーザーの目線に立ったものづくりを行っていきたいと考えています。</p>
            </div>
        </div>
        <div class="card fluid">
            <div class="section">
                <h4>aaa</h4>
                <p>私は企画もできるエンジニアです。前職では1000名を超える社員数を持つ会社規模の中、自分のアイデアがベースとなった自社サイトをいくつも生み出してきました。前職では企画部門と開発部門に大きな境目はなく、エンジニアという肩書きの私も企画会議に出席し、月30本ものアイデア出しを2年間続けていました。苦しい日々ではありましたが自分の頭をひねり続ける中で、ユーザー視点がとても養われたと感じています。私が提案した企画が通り開発に至ったサイトも複数あり、社内表彰を過去に2回受賞しています。今後も自社ブランドをもつ貴社でユーザーの目線に立ったものづくりを行っていきたいと考えています。</p>
            </div>
        </div>
        <div>
            #PHP #JAVA #HTML #CSS
        </div>
     </div>
  </div>
</div>
