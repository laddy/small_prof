<!doctype html>

<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="description" content="社内用プロフィール紹介">
<meta name="author" content="SitePoint">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>社内用プロフィール</title>

<link rel="stylesheet" href="https://gitcdn.link/repo/Chalarangelo/mini.css/master/dist/mini-default.min.css">
<link rel="stylesheet" href="/dist/mini-default.min.css">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
<![endif]-->
</head>

<body>

<header class="sticky">
    <div class="col-sm col-md-10 col-md-offset-1">
        <a href="#" role="button">Home</a>
        <a href="#" role="button">About</a>
        <a href="#" role="button">Contact</a>
        <form action="<?=base_url();?>/auth/login">
            <div class="input-group">
              <label for="username">username</label> 
              <input type="email" value="" id="username" placeholder="username">
            </div>
            <div class="input-group">
              <label for="pwd">password</label> 
              <input type="password" value="" id="pwd" placeholder="password">
            </div>

            <div class="input-group">
                <input type="submit">
            </div>
        </form>
    </div>
</header>



