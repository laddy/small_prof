<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function index()
    {
        $this->load->view('header');
        $this->load->view('top');
        $this->load->view('footer');

        return ;
    }


    /*
     * ユーザのプロフィールページを表示する
     */
    public function prof($user_id = '')
    {
        $user = $this->db->where(['user_id' => $user_id])
            ->get('users')
            ->first_row();
        if ( 0 == count($user) ) {
            redirect(base_url());
        }
        
        $user_id = $user->id;
        $result = $this->db->where([ 'user_id' => $user_id ])
            ->order_by('sort', 'ASC')
            ->get('profile')->result();
        
        $data = [
            'user_id' => $user->user_id,
            'profile' => $result,
        ];
        
        $this->load->view('header');
        $this->load->view('prof', $data);
        $this->load->view('footer');
       
        return ;
    }


}
